import { Html, Head, Main, NextScript } from 'next/document';

export default function Document() {
  return (
    <Html className="bg-barmenia" lang="de">
      <Head />
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
