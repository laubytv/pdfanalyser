import type { NextApiRequest, NextApiResponse } from 'next';
import { OpenAIEmbeddings } from 'langchain/embeddings/openai';
import { PineconeStore } from 'langchain/vectorstores/pinecone';
import { createChainContext } from '@/core/utils/createChainContext';
import { pinecone } from '@/core/utils/pinecone-client';
import {
  PINECONE_INDEX_NAME,
  PINECONE_NAME_SPACE,
} from '@/core/config/pinecone';
import { ChainValues } from 'langchain/schema';
import { ConversationalRetrievalQAChain } from 'langchain/chains';
import { VectorOperationsApi } from '@pinecone-database/pinecone/dist/pinecone-generated-ts-fetch';

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  const { question, history } = req.body;

  console.log('Frage: ', question);

  if (req.method !== 'POST') {
    res.status(405).json({ error: 'Methode nicht erlaubt' });
    return;
  }

  if (!question) {
    return res.status(400).json({ message: 'Keine Frage im Request' });
  }

  const sanitizedQuestion = question.trim().replaceAll('\n', ' ');

  try {
    const index: VectorOperationsApi = pinecone.Index(PINECONE_INDEX_NAME);

    const vectorStore: PineconeStore = await PineconeStore.fromExistingIndex(
      new OpenAIEmbeddings({}),
      {
        pineconeIndex: index,
        textKey: 'text',
        namespace: PINECONE_NAME_SPACE,
      },
    );

    const chain: ConversationalRetrievalQAChain =
      createChainContext(vectorStore);

    const response: ChainValues = await chain.call({
      question: sanitizedQuestion,
      chat_history: history || [],
    });

    console.log('Response: ', response);
    res.status(200).json(response);
  } catch (error: any) {
    console.log('Fehler: ', error);
    res
      .status(500)
      .json({ error: error.message || 'Etwas ist schief gegangen' });
  }
}
