import { PineconeClient } from '@pinecone-database/pinecone';

if (!process.env.PINECONE_ENVIRONMENT) {
  throw new Error('Dein Pinecone Environment fehlt!');
}
if (!process.env.PINECONE_API_KEY) {
  throw new Error('Dein Pinecone API Key fehlt!');
}

async function initialisePineconeDatabase(): Promise<PineconeClient> {
  try {
    const pinecone: PineconeClient = new PineconeClient();

    await pinecone.init({
      environment: process.env.PINECONE_ENVIRONMENT ?? '', //this is in the dashboard
      apiKey: process.env.PINECONE_API_KEY ?? '',
    });

    return pinecone;
  } catch (error) {
    console.log('Du hast einen Fehler: ', error);
    throw new Error('Pinecone Client konnte nicht initialisiert werden!');
  }
}

export const pinecone: PineconeClient = await initialisePineconeDatabase();
