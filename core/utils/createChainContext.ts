import { OpenAI } from 'langchain/llms/openai';
import { PineconeStore } from 'langchain/vectorstores/pinecone';
import { ConversationalRetrievalQAChain } from 'langchain/chains';

export const createChainContext = (store: PineconeStore) => {
  const openAi: OpenAI = new OpenAI({
    temperature: 0,
    modelName: 'gpt-4',
  });

  const conversationalRetrievalQAChain: ConversationalRetrievalQAChain =
    ConversationalRetrievalQAChain.fromLLM(openAi, store.asRetriever(), {
      qaTemplate: PROMPT,
      questionGeneratorTemplate: TEMPLATE_FOR_PROMPT,
      returnSourceDocuments: true,
    });
  return conversationalRetrievalQAChain;
};

const TEMPLATE_FOR_PROMPT = `Formuliere das folgende Gespräch und die anschließende Frage so um, dass es eine eigenständige Frage ist.

Chat Historie:
{chat_history}
Nachbereitung der Eingabe: {question}
Eigenständige Frage:`;

const PROMPT = `Du bist ein hilfreicher KI-Assistent von der Barmenia. Beantworte die Frage am Ende anhand der folgenden Informationen aus einer PDF.
Wenn du die Antwort nicht weißt, sagst du einfach, dass du es nicht weißt. Versuche  NICHT dir eine Antwort auszudenken.
Wenn die Frage nicht mit der PDF zusammenhängt, antworte höflich, dass du darauf eingestellt bist, nur Fragen zu beantworten, die mit einer PDF zusammenhängen.

{context}

Frage: {question}
Hilfreiche Antwort ab hier:`;
