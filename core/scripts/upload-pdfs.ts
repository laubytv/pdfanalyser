import { RecursiveCharacterTextSplitter } from 'langchain/text_splitter';
import { OpenAIEmbeddings } from 'langchain/embeddings/openai';
import { PineconeStore } from 'langchain/vectorstores/pinecone';
import { pinecone } from '@/core/utils/pinecone-client';
import { PDFLoader } from 'langchain/document_loaders/fs/pdf';
import {
  PINECONE_INDEX_NAME,
  PINECONE_NAME_SPACE,
} from '@/core/config/pinecone';
import { DirectoryLoader } from 'langchain/document_loaders/fs/directory';
import { VectorOperationsApi } from '@pinecone-database/pinecone/dist/pinecone-generated-ts-fetch';

const filePath: string = 'pdfs';

export const run = async (): Promise<void> => {
  try {
    const directoryLoader: DirectoryLoader = new DirectoryLoader(filePath, {
      '.pdf': (path: string) => new PDFLoader(path),
    });

    const rawDocs: any = await directoryLoader.load();

    const textSplitter: RecursiveCharacterTextSplitter =
      new RecursiveCharacterTextSplitter({
        chunkSize: 1000,
        chunkOverlap: 200,
      });

    const pdfs: any = await textSplitter.splitDocuments(rawDocs);

    console.log('VectorStore in Pinecone wird erzeugt');

    const embeddings: OpenAIEmbeddings = new OpenAIEmbeddings();
    const index: VectorOperationsApi = pinecone.Index(PINECONE_INDEX_NAME);

    await PineconeStore.fromDocuments(pdfs, embeddings, {
      pineconeIndex: index,
      namespace: PINECONE_NAME_SPACE,
      textKey: 'text',
    });
  } catch (error) {
    console.log('Fehler: ', error);
    throw new Error('PDFs konnten nicht hochgeladen werden!');
  }
};

(async () => {
  await run();
  console.log('Hochladen abgeschlossen');
})();
