/**
 * Änder den Namespace zu dem Namespace auf Pinecone
 */

if (!process.env.PINECONE_INDEX_NAME) {
  throw new Error('Fehlender Pinecone-Indexname in der .env-Datei!');
}

const PINECONE_INDEX_NAME = process.env.PINECONE_INDEX_NAME ?? '';

const PINECONE_NAME_SPACE = 'pdf-test'; //namespace is optional for your vectors

export { PINECONE_INDEX_NAME, PINECONE_NAME_SPACE };
